package com.example;

public class QuickSort {
    public static int[] sort(int[] array) {
        sortHelper(array, 0, array.length - 1);
        return array;
    }

    private static void sortHelper(int[] array, int low, int high) {
        if (low < high) {
            int partitionIndex = partition(array, low, high);
            sortHelper(array, low, partitionIndex - 1);
            sortHelper(array, partitionIndex + 1, high);
        }
    }

    private static int partition(int[] array, int low, int high) {
        int pivot = array[high];
        int i = low - 1;
        for (int j = low; j < high; j++) {
            if (array[j] <= pivot) {
                i++;
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
        }
        int temp = array[i + 1];
        array[i + 1] = array[high];
        array[high] = temp;
        return i + 1;
    }
}
