package com.example;

import java.util.Arrays;

public class App {
    public static void main(String[] args) {
        int[] arrayToSort = {5, 2, 8, 1, 6};
        System.out.println("Original Array: " + Arrays.toString(arrayToSort));

        int[] bubbleSortedArray = BubbleSort.sort(Arrays.copyOf(arrayToSort, arrayToSort.length));
        int[] selectionSortedArray = SelectionSort.sort(Arrays.copyOf(arrayToSort, arrayToSort.length));
        int[] quickSortedArray = QuickSort.sort(Arrays.copyOf(arrayToSort, arrayToSort.length));

        System.out.println("Bubble Sorted Array: " + Arrays.toString(bubbleSortedArray));
        System.out.println("Selection Sorted Array: " + Arrays.toString(selectionSortedArray));
        System.out.println("Quick Sorted Array: " + Arrays.toString(quickSortedArray));
    }

}
