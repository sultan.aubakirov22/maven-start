package com.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Arrays;


class BubbleSortTest {

    @ParameterizedTest
    @CsvSource({"5, 2, 8, 1, 6", "3, 1, 4, 1, 5", "10, 9, 8, 7, 6", "1, 2, 3, 4, 5"})
    void testBubbleSort(int a, int b, int c, int d, int e) {
        int[] inputArray = {a, b, c, d, e};
        int[] arr = {a, b, c, d, e};
        Arrays.sort(arr);
        Assertions.assertArrayEquals(arr, BubbleSort.sort(inputArray));
    }
}

class QuickSortTest {

    @ParameterizedTest
    @CsvSource({"5, 2, 8, 1, 6", "3, 1, 4, 1, 5", "10, 9, 8, 7, 6", "1, 2, 3, 4, 5"})
    void testQuickSort(int a, int b, int c, int d, int e) {
        int[] inputArray = {a, b, c, d, e};
        int[] arr = {a, b, c, d, e};
        Arrays.sort(arr);
        Assertions.assertArrayEquals(arr, BubbleSort.sort(inputArray));
    }
}

class SelectionSortTest {

    @ParameterizedTest
    @CsvSource({"5, 2, 8, 1, 6", "3, 1, 4, 1, 5", "10, 9, 8, 7, 6", "1, 2, 3, 4, 5"})
    void testSelectionSort(int a, int b, int c, int d, int e) {
        int[] inputArray = {a, b, c, d, e};
        int[] arr = {a, b, c, d, e};
        Arrays.sort(arr);
        Assertions.assertArrayEquals(arr, BubbleSort.sort(inputArray));
    }
}
